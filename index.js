const cluster = require('cluster');
const EventEmitter = require('events').EventEmitter;
const CPU_COUNT = require('os').cpus().length;

const DEFAULTS = {
  clusterLifetime: Infinity,
  gracePeriod: 5000,
  workers: CPU_COUNT
};

let instance;

class Flock {
  constructor(startFn = Function, options = {}) {
    if (instance) return instance;
    instance = this;

    if (typeof startFn === 'object') instance.options = Object.assign({}, DEFAULTS, startFn);
    else instance.options = Object.assign({}, DEFAULTS, options);
    instance.options.startFn = instance.options.startFn || startFn;
    if (!instance.options.startFn || typeof instance.options.startFn !== 'function') throw new Error('startFn function required');

    instance.emitter = new EventEmitter();
    instance.running = false;
    instance.runUntil = Date.now() + options.clusterLifetime;
  }

  start() {
    if (cluster.isWorker) return instance.options.startFn(cluster.worker.id);
    instance.running = true;
    instance._listen();
    instance._fork();
  }

  restart() {
    if (instance.running) {
      instance._shutdown();
    }
    setTimeout(instance.start, instance.options.gracePeriod).unref();
  }

  kill() {
    instance._kill();
  }

  isRunning() {
    return instance.running;
  }

  _listen() {
    cluster.on('exit', instance._revive);
    instance.emitter.once('shutdown', instance._kill.bind(this));
    process
      .on('SIGINT', instance._proxySignal.bind(this))
      .on('SIGTERM', instance._proxySignal.bind(this));
  }

  _fork() {
    for (let i = 0; i < instance.options.workers; i++) {
      cluster.fork();
    }
  }

  _proxySignal() {
    instance.emitter.emit('shutdown');
  }

  _shutdown() {
    instance.running = false;
    for (let id in cluster.workers) {
      cluster.workers[id].process.kill();
    }
  }

  _kill() {
    instance._shutdown();
    setTimeout(instance._forceKill, instance.options.gracePeriod).unref();
  }

  _forceKill() {
    for (let id in cluster.workers) {
      cluster.workers[id].kill();
    }
    process.exit();
  }

  _revive() {
    if (instance.running && Date.now() < instance.runUntil) cluster.fork();
  }


}

module.exports = Flock;
