const chai = require('chai');
const expect = chai.expect;
const Flock = require('../index');
const sinon = require('sinon');

describe('node-flock', function(){
  let flock;
  let sandbox;

  beforeEach(function(){
  	sandbox = sinon.sandbox.create();
  });

  afterEach(function(){
  	sandbox.restore();
  });

  it('should start with only a start function', function(){
    let startFn = sandbox.spy();
    const flock = new Flock(startFn);

    flock.start();

    expect(flock).to.exist;
    expect(startFn.called).to.be.ok;
  });

  it('should start with a start function and config object', function(){
    let startFn = sandbox.spy();
    const flock = new Flock(startFn, { workers: 1 });

    flock.start();

    expect(flock).to.exist;
    expect(startFn.called).to.be.ok;
  });

});
