node-flock
==========

This is a simple cluster manager for NodeJS. It features a method to restart a running cluster. It is heavily inspired by [throng](https://github.com/hunterloftis/throng).

It is written in es2015 so it requires a NodeJS version which supports es2015 classes natively.

# Installation

```shell
npm install node-flock
```

# Simple Use

```Javascript
const Flock = require('node-flock');
const flock = new Flock(start);

function start() {}

flock.start();

```

# Use with Start Function and Config

```Javascript
const Flock = require('node-flock');
const flock = new Flock(start, {
  clusterLifetime: Infinity, // milliseconds to keep cluster alive
  gracePeriod: 4000 // milliseconds to shutdown or restart cluster
  workers: 1, // number of workers
});

function start() {}

flock.start();
```

# Use with Config

```Javascript
const Flock = require('node-flock');
const flock = new Flock({
  clusterLifetime: Infinity, // milliseconds to keep cluster alive
  gracePeriod: 4000, // milliseconds to shutdown or restart cluster
  startFn: start,
  workers: 1 // number of workers
});
```

# Use with Restart

```Javascript
const Flock = require('node-flock');
const flock = new Flock(start);

function start() {}

flock.start();

setTimeout(flock.restart, 2000);
```
